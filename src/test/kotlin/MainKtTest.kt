import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class MainKtTest {
    private lateinit var game:Game

    @BeforeEach
    fun setup() {
        game = Game()
    }

    @Test
    fun regularCellReturnsStayinSpace() {
        assertCellReturnString(game.getRule(2), "Stay in space 2")
    }

    @Test
    fun brigdeCellReturnsTheBridgeGotospace12() {
        assertCellReturnString(game.getRule(6), "The Bridge: Go to space 12")
    }

    @Test
    fun divBySixCellsReturnsMovetwospacesforward() {
        assertCellReturnString(game.getRule(12), "Move two spaces forward.")
    }

    private fun assertCellReturnString(result: String, expected: String) {
        assertEquals(result, expected)
    }
}
