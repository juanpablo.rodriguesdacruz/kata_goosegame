import Constantes.FINAL_PLACE

fun main(args: Array<String>) {
    var actualPlace = 0

    while (actualPlace <= FINAL_PLACE) {
        getRule(actualPlace++)
    }
}

private fun getRule(actualPlace: Int) {
    when {
        checkPlayerAreInTheBridge(actualPlace) -> println("The Bridge: Go to space 12")
        checkPlayerAreInTeleportCell(actualPlace) -> println("Move two spaces forward.")
        else -> println("Stay in space $actualPlace")
    }
}

private fun checkPlayerAreInTheBridge(actualPlace: Int) = actualPlace == 6

private fun checkPlayerAreInTeleportCell(actualPlace: Int) = actualPlace % 6 == 0 && actualPlace !=6

object Constantes {
    const val FINAL_PLACE = 63
}