class Game {
    fun getRule(position: Int): String {
        val string = when {
            isBridgeCell(position) -> "The Bridge: Go to space 12"
            isDivBySixCell(position) -> "Move two spaces forward."
            else -> "Stay in space $position"
        }
        return string
    }

    private fun isDivBySixCell(position: Int) = position % 6 == 0

    private fun isBridgeCell(position: Int) = position == 6
}
